#include "arithmetic.h"

int main(int argc, char const *argv[])
{
    int x,y;
    printf("Enter 2 numbers: ");
    scanf("%d%d",&x,&y);
    printf("The results of all arithmetic operations on 2 inputs are: %d %d %d %f\n",add(x,y),subtract(x,y),
        multiply(x,y),divide(x,y));
    return 0;
}
