#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h> /*change to <sys/fcntl.h> for System V */
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
/* 
 * This file initialises a struct named record
 * It contains 4 usernames, from userA to userD
 * Then it writes to recordfile 
 */
struct record 
{
    int userid;
    char username[6];
};

//char *usernames[] = { "userA", "userB", "userC", "userD"};

int main(int argc, char *argv[])
{
    int i, infile;
    struct record eachrec;

    // Open the file (recordfile)
   
    if ((infile = open("recordfile", O_RDONLY)) < 0)
    {
        perror("recordfile");
        exit(1);
    }
    off_t current;
    // Print second
    {
        char name[12];
        lseek(infile , sizeof(struct record), SEEK_SET);
        int tmp = read(infile, name, sizeof(struct record));
        for (int i=4;i<9;i++) printf("%c",name[i]);
        printf("\n");
        lseek(infile,0,SEEK_SET);
    }
    
    // Print fourth
    {
        {
        char name[12];
        lseek(infile , 3*sizeof(struct record), SEEK_SET);
        int tmp = read(infile, name, sizeof(struct record));
        for (int i=4;i<9;i++) printf("%c",name[i]);
        printf("\n");
        lseek(infile,0,SEEK_SET);
    }
    }
    // Print first
    {
        char name[12];
        //lseek(infile , sizeof(struct record), SEEK_SET);
        int tmp = read(infile, name, sizeof(struct record));
        for (int i=4;i<9;i++) {
            //printf("%c",name[i]);
        
            printf("\n");
            printf("%s",name);
        //lseek(infile,0,SEEK_SET);
        }
    }
    // Print third
    {
        char name[12];
        lseek(infile , 2*sizeof(struct record), SEEK_SET);
        int tmp = read(infile, name, sizeof(struct record));
        for (int i=4;i<9;i++) printf("%c",name[i]);
        printf("\n");
        lseek(infile,0,SEEK_SET);
    }

    close(infile);
    exit(0);
}
  

