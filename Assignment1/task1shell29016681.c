/*
Student Name: Thach K. Tran
Student ID: 29016681
Start Date: 14/08/2018
Finish Date: 21/08/2018 */

/*
This is the main driver of the shell, which reacts to user input
by calling the relevant functions.
Functionalities include showing all files and their type in the 
current directory, executing and viewing files specified by user command.

Please refer to task1func.c for implementation of the functions.
*/
#include "task1func.h"
#define q "q:"

int main(int argc, char const *argv[])
{
    /* code */
    printCurDir();
    printDirContent(".");
    while (1)
    {

        char command[PATH_MAX+3];
        // Get user input
        fgets(command,PATH_MAX+3,stdin);
        // Remove newline char
        command[strlen(command)-1] = '\0';
        
        // Exit the shell
        if (!strcmp(command,q)) break;
        char path[PATH_MAX];
        strncpy(path,command+2,strlen(command));

        // Change dir
        if (command[0] == 'd' && changeDirAndPWD(path))
        {
            printCurDir();
            printDirContent(".");
        }

        // Execute file
        else if (command[0] == 'x')
        {
            execute(path);
        }

        // View file
        else if (command[0] == 'v')
        {
            view(path);
        }
    }
    return 0;
}
