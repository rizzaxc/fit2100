/* Implementation of functions used by the main driver*/
#include "task2func.h"

int printDirContent(char *dir)
{
    struct dirent *de;  // Pointer for directory entry
 
    // opendir() returns a pointer of DIR type. 
    DIR *dr = opendir(dir);
 
    if (dr == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("<errorMessage.visible true>\
        <errorMessage {Error: You don't have permission to view this directory!}>");
        return 0;
    }
 
    // Refer http://pubs.opengroup.org/onlinepubs/7990989775/xsh/readdir.html
    // for readdir()
    // Iteratively read each item in the directory
    while ((de = readdir(dr)) != NULL)
    {
        //Don't display . and .. since they're hidden
        if ((strcmp(de->d_name,".") != 0) && (strcmp(de->d_name,"..") != 0))
        {
            // Categorize the item and set the appropriate onclick command
            char fileType[16];
            char onClickCommand[NAME_MAX+3];
            if (de->d_type == 4) 
            {
                strcpy(fileType,DIRECTORY);
                strcpy(onClickCommand,"d:");
            }
            else if ((de->d_type == 8) && !access(de->d_name,X_OK))
            {
                strcpy(fileType,EXEC);
                strcpy(onClickCommand,"x:");
            }
            else 
            {
                strcpy(fileType,TEXT);
                strcpy(onClickCommand,"v:");
            }
            strcat(onClickCommand,de->d_name);

            //Spawn a box, containing the type image, file name and onclick listener
            printf("<box onclick=<putln {%s}> inline=true spacing-right=20 spacing-top=10\
            border=0 color={black} <box size=60 border=0 bold=true color={green} text-align={center} %s>\
            size=20 width=30,{%} {%s} >",onClickCommand,fileType,de->d_name);
        }
    }
    printf("\n");
    closedir(dr);    
    return 0;
}

int changeDirAndPWD(char *dir)
{
    // Error when change dir
    if (chdir(dir))
    {
        if (errno == EACCES) printf("<errorMessage.visible true>\
        <errorMessage {Error: You don't have permission to view this directory!}>");
        return 0;
    }
    char cwd[PATH_MAX];
    getcwd(cwd,sizeof(cwd));
    setenv("PWD",cwd,1);
    return 1;
}


int printCurDir()
{
    char cwd[PATH_MAX];
    if (getcwd(cwd,sizeof(cwd)) != NULL) 
    {
        printf("<curDir.clear>");
        printf("<curDir {Current Directory: %s}>",cwd);
        printf("</n>");
        return 0;
    }
    return 1;

}

void execute(char *path)
{
    if (!access(path,X_OK))
    {
        char fullPath[PATH_MAX+2] ="./";
        strcat(fullPath,path); // Complete the path to the file
        char command[PATH_MAX+16] = "xterm -e ";
        strcat(command,fullPath); // Concat the path to the command
        strcat(command," &"); // Run program in the background to not block the current process

        if (system(command) == -1) printf("<errorMessage.visible true>\
        <errorMessage {Error: File run unsuccessfully!}>");       

    }
    
    else if (errno == EACCES) printf("<errorMessage.visible true>\
    <errorMessage {Error: You don't have permission to execute this file!}>");
}

void view(char *path)
{
    if (!access(path,R_OK))
    {
        // Check if file small enough
        struct stat statBuf;
        stat(path,&statBuf);
        if (statBuf.st_size <= MAX_SIZE)
        {
            char command[PATH_MAX+16] = "xterm -e less ";
            strcat(command,path); // Concat the path to the command
            strcat(command," &"); // View the file in the background

            if (system(command) == -1) printf("<errorMessage.visible true>\
            <errorMessage {Error: File viewed unsuccessfully!}>");       
        }
        else printf("<errorMessage.visible true>\
        <errorMessage {Error: File is too big to be viewed this way!}>");
    }
    else if (errno == EACCES) printf("<errorMessage.visible true>\
    <errorMessage {Error: You don't have permission to view this file!}>");
}