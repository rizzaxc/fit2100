/*Functions, libraries and relevant constants are declared here*/

#include <stdio.h>
#include <dirent.h>
#include <limits.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>

// Max file size allowed to use view
#define MAX_SIZE 500000

int printDirContent(char *dir);
int changeDirAndPWD(char *dir);
int printCurDir();
void execute(char *path);
void view(char *path);