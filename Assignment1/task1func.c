/*Implementation of all functions used in the main driver */

#include "task1func.h"

int printDirContent(char *dir)
{
    struct dirent *de;  // Pointer for directory entry
 
    // opendir() returns a pointer of DIR type. 
    DIR *dr = opendir(dir);
 
    if (dr == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory" );
        return 0;
    }

    printf("***Directory listing follows***\n\n");
 
    // Refer http://pubs.opengroup.org/onlinepubs/7990989775/xsh/readdir.html
    // for readdir()
    // Iteratively read each item in the directory
    while ((de = readdir(dr)) != NULL)
    {
        //Don't display . and .. since they're hidden
        if ((strcmp(de->d_name,".") != 0) && (strcmp(de->d_name,"..") != 0))
        {
            // Categorize the item
            char fileType[16];
            if (de->d_type == 4) strcpy(fileType,"Directory");
            else if ((de->d_type == 8) && !access(de->d_name,X_OK))
                strcpy(fileType,"Executable File");
            else strcpy(fileType,"Ordinary File");

            printf("%s: %s\n",fileType,de->d_name);
        }
    }
    printf("\n");
    closedir(dr);    
    return 0;
}

int changeDirAndPWD(char *dir)
{
    // Error when change dir
    if (chdir(dir))
    {
        if (errno == EACCES) printf("Error: You don't have perrmission to view this directory! \n");
        return 0;
    }

    char cwd[PATH_MAX];
    getcwd(cwd,sizeof(cwd));
    setenv("PWD",cwd,1);
    return 1;
}


int printCurDir()
{
    char cwd[PATH_MAX];
    if (getcwd(cwd,sizeof(cwd)) != NULL) 
    {
        printf("Current working directory: %s\n",cwd);
        return 0;
    }
    return 1;

}

void execute(char *path)
{
    if (!access(path,X_OK))
    {
        char fullPath[PATH_MAX+2] ="./";
        strcat(fullPath,path); // Complete the path to the file
        char command[PATH_MAX+16] = "xterm -e ";
        strcat(command,fullPath); // Concat the path to the command
        strcat(command," &"); // Run program in the background to not block the current process

        if (system(command) != -1) printf("File run successfully!\n\n");
        else printf("File run unsuccessfully!\n\n");
    }
    
    else if (errno == EACCES) printf("Error: You don't have permission to execute this file!\n\n");
}

void view(char *path)
{
    if (!access(path,R_OK))
    {
        // Check if file small enough
        struct stat statBuf;
        stat(path,&statBuf);
        if (statBuf.st_size <= MAX_SIZE)
        {
            char command[PATH_MAX+16] = "xterm -e less ";
            strcat(command,path); // Concat the path to the command
            strcat(command," &"); // View the file in the background

            if (system(command) != -1) printf("File viewed successfully!\n\n");
            else printf("File viewed unsuccessfully!\n\n");        
        }
        else printf("Error: The file is too big to be viewed this way!\n\n");
    }
    else if (errno == EACCES) printf("Error: You don't have permission to view this file!\n\n");
}