/*
Student Name: Thach K. Tran
Student ID: 29016681
Start Date: 19/08/2018
Finish Date: xx/08/2018 */

/*
This is the main driver of the shell, which reacts to input from
onclick listener
*/
#include "task2func.h"
#define q "q:"

// Set up the general view of GUI
void setup()
{
    printf("<program.clear>");
    printf("<program.background-color {white}>");
    printf("<goUp:box {Go to parent directory} top=5 size=30 bold=true\
    color={black} onclick=<putln {d:..}> >");

    // Message box set to invisible; if clicked become invisible
    printf("<errorMessage:box {Error: None} spacing-left=10 top=5 size=30\
    italic=true bold=true visible=false color={blue} onclick=<errorMessage.visible false> >");
    printf("</n>");

    // Box to display current directory
    printf("<curDir:box spacing-top=15 size=30 color={black}>");

    // Exit button at top right corner
    printf("<closeButton:button {Close} inline=false right=0 top=0 size=30\
    background-color={black} onclick=<putln {q:}> >");

    // Exit program gracefully
    printf("<program.onexit.push <putln {q:}> >");

}

int main(int argc, char const *argv[])
{
    /* code */
    setup();
    printCurDir();
    printDirContent(".");
    while (1)
    {
        // Get input
        char command[PATH_MAX+3];
        fgets(command,PATH_MAX+3,stdin);
        // Remove newline char
        command[strlen(command)-1] = '\0';

        // Exit the shell
        if (!strcmp(command,q)) break;
        char path[PATH_MAX];
        strncpy(path,command+2,strlen(command));

        // Change dir
        if (command[0] == 'd' && changeDirAndPWD(path))
        {
            setup();
            printCurDir();
            printDirContent(".");
        }

        // Execute file
        else if (command[0] == 'x')
        {
            execute(path);
        }

        // View file
        else if (command[0] == 'v')
        {
            view(path);
        }
    }
    return 0;
}
