#include <stdio.h>

int main(void)
{
    char str1[] = "FIT2100 Operating System";
    char str2[] = "Week 4 Practical";
    char *ptr = NULL;

    ptr = str1;
    printf("%s\n",ptr);

    ptr = str2;
    //printf("%s\n",*ptr); --> This line is wrong
    // 2 options: print the memory location or the string
        //printf("%p\n",ptr); --> this will print the memory location
    printf("%s\n",ptr); // this will print the string
    
    return 0;
}