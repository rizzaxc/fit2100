#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h> /*change to <sys/fcntl.h> for System V */
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
/* 
 * This file initialises a struct named record
 * It contains 4 usernames, from userA to userD
 * Then it writes to recordfile 
 */
struct record 
{
    int userid;
    char username[6];
};

char *usernames[] = { "userA", "userB", "userC", "userD"};

int main(int argc, char *argv[])
{
    int i, outfile;
    struct record eachrec;

    // Open the file (recordfile) for writing
   
    if ((outfile = open("recordfile", O_WRONLY | O_CREAT, 0664)) < 0)
    {
        perror("recordfile");
        exit(1);
    }

    for (i = 3; i>=0; i--)
    {
    /*
     *Create a new record
     */
        eachrec.userid = i;
        strcpy(eachrec.username, usernames[i]);

    /*
     *Write the record into the file
     Write from right to left, assigning spaces based on blocks of struct record
     */
        lseek(outfile , (long) i * sizeof(struct record), SEEK_SET);
        int tmp = write(outfile, &eachrec, sizeof(struct record));
        printf("%d\n",tmp);
    }

    close(outfile);
    exit(0);
}
  

