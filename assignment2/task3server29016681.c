/* 
 * Student Name: Thach K. Tran
 * StudentID: 29016681
 * Start Date: 26/09/2018
 * End Date: 28/09/2018
 */


/* 
 * Server implimentation for communication between a server and at most 5 clients
 * The server creates a connection and responses to clients connected to it
 * It prints out data from the client to stdout along with the client names
 * This has a shutdown function
 */


#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SOCKETNAME "/tmp/a2-29016681.socket"
#define MAX_SIZE 1024
#define EOL "</n>"
#define MAX_CONNECTION 5

void createWindow(int connection);
void serve(int *nsock,int i,int *curNoConnection);

int main(int argc, char const *argv[])
{
    int n,sock,nsock[MAX_SIZE],len,tmp;
    int iterator = 0,curNoConnection = 0;
    struct sockaddr_un name;
    fd_set set, activeSet;

    /* Create a socket */
    if ((sock = socket(AF_UNIX,SOCK_STREAM,0)) < 0)
    {
        perror("server: socket");
        exit(1);
    }

    /* Create the address of the server */
    memset(&name,0,sizeof(struct sockaddr_un));
    name.sun_family = AF_UNIX;
    strcpy(name.sun_path,SOCKETNAME);
    len = sizeof(name.sun_family) + strlen(name.sun_path);

    /* Remove any previous socket */
    unlink(name.sun_path);

    /* Bind socket to the address */
    if (bind(sock,(struct sockaddr*) &name,SUN_LEN(&name)) < 0)
    {
        perror("server: bind");
        exit(1);
    }

    /* Listen for connections */
    if (listen(sock,5) < 0)
    {
        perror("server: listen");
        exit(1);
    }

    /* Exit button */
    printf("<button {Shutdown</n>} onclick=<putln {Q}>>\n");
    /* Error box */
    printf("<errorBox:box visible=false {ERROR: Too many connections}></n>");

    /* Accept multiple connections */
    FD_ZERO(&set);
    FD_SET(sock,&set);
    FD_SET(0,&set);
    while (1)
    {
        activeSet = set;
        if (select(FD_SETSIZE,&activeSet,NULL,NULL,NULL) < 0)
        {
            perror("server: select");
            exit(1);
        }
        
        
        /* There's client waiting to be accepted */
        if (FD_ISSET(sock,&activeSet))
        {
            int newConnection = accept(sock,(struct sockaddr *) &name,&len); // Accept the connection
            if (newConnection < 0)
            {
                perror("server: accept");
                exit(1);
            }
            /* If there's still room for this connection, add it to the set */
            if (curNoConnection < MAX_CONNECTION)
            {
                printf("<errorBox.visible false>"); // Hide the error message as needed
                curNoConnection++; // Inc the count of connections
                nsock[iterator] = newConnection; // Add its ID to list of connection
                iterator++; // Inc the iterator to point to the next slot
                FD_SET(newConnection,&set); // Add its ID to list of file descriptors           
                createWindow(newConnection); // Create a GUI window for the client
            }
            /* If no room, show the error message and close it immediately */
            else 
            {
                printf("<errorBox.visible true>");
                close(newConnection);
            }
        }

        /* 
        * Iterate through the list of saved clients
        * Check if each of them is ready
        * If yes, serve
        */
        for (int i=0;i<=iterator;i++)
        {
            /* The connection needs to be both open and ready */
            if (nsock[i] != -1 && FD_ISSET(nsock[i],&activeSet))
            {
                printf("<errorBox.visible false>"); // Hide the error message as needed
                serve(nsock,i,&curNoConnection);
            }
        }
 
        /* If user presses quit, exit the server */
        if (FD_ISSET(0,&activeSet)) break;
    }
    
    /* Close all the connections */
    for (int i=iterator;i>=0;i--)
    {
        if (nsock[i] != -1) close(nsock[i]);
    }
    close(sock);
    exit(0);
}

/* 
 * This function creates the GUI window for the client indicated by arg `connection`
 * The window is numbered after the client to distoguish between clients
 */
void createWindow(int connection)
{
    char buffer[MAX_SIZE], clientName[MAX_SIZE];
    int tmp;

    /* Setup a GUI window */
    printf("<window%d:box width=100,{%%} {",connection);

    tmp = recv(connection,buffer,MAX_SIZE,0); // Read from socket
    strncpy(clientName,buffer,tmp); // Store the name in the string clientName
    clientName[tmp] = '\0'; // Null terminate the name string
    printf("<box color={yellow} background-color={blue} bold=true\
    width=100,{%%} {%s}>",clientName); // Create a box to hold the client name

    printf("<contentBox:box border=0 width=100,{%%} {}>"); // Container to hold data

    /* Close the GUI window */
    printf("}>");

}


/* 
 * This function will fire after data from its client is ready
 * And will print the data to stdout
 * If a client disconnects, this will also display the appropriate info on its window
 * 
 * Args: `nsock` is the client list, `i` is the client's index within that list, 
 *       `curNoConnection` is current no of connections
 */
void serve(int *nsock,int i,int *curNoConnection)
{
    char buffer[MAX_SIZE];
    int connection = nsock[i]; // Get the client we need to serve

    int tmp = recv(connection,buffer,MAX_SIZE,0); // Attempt to read from it
    
    /* Not receive any data, connection not open anymore */
    if (!tmp)
    {
        nsock[i] = -1; // Mark the connection as closed
        (*curNoConnection)--; // Decrease the number of current connections
        printf("<window%d.contentBox {<window%d.contentBox.pull> CLIENT DISCONNECTED}>",connection,connection); // Display dc message
        return;
    }

    /* Otherwise, proceed to print */
    buffer[tmp] = '\0'; // Null terminate the data
    printf("<window%d.contentBox {<window%d.contentBox.pull> %s}>",connection,connection,buffer); // Print data received
    
}
