/* 
 * Student Name: Thach K. Tran
 * StudentID: 29016681
 * Start Date: 25/09/2018
 * End Date: 25/09/2018
 */


/* 
 * Client implimentation for communication between a server and a client
 * The client tries to connect to a socket and send its name and data from stdin over
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SOCKETNAME "/tmp/a2-29016681.socket"
#define MAX_SIZE 1024
#define Q "quit\n"
#define EOL "</n>"

int main(int argc, char const *argv[])
{
    int n,sock,len;
    struct sockaddr_un name;

    /* Create a socket */
    if ((sock = socket(AF_UNIX,SOCK_STREAM,0)) < 0)
    {
        perror("client: socket");
        exit(1);
    }

    /* Create the address of the server */
    memset(&name,0,sizeof(struct sockaddr_un));
    name.sun_family = AF_UNIX;
    strcpy(name.sun_path,SOCKETNAME);
    len = sizeof(name.sun_family) + strlen(name.sun_path);

    /* Connect to the server */
    if (connect(sock,(struct sockaddr *) &name,SUN_LEN(&name)) < 0)
    {
        perror("client: connect");
        exit(1);
    }

    /* First send the name of the client program to socket*/
    send(sock,argv[0],strlen(argv[0]),0);
    
    /* While the user hasn't quit, send data from stdin to socket */
    while (1)
    {
        char buffer[MAX_SIZE];
        fgets(buffer,MAX_SIZE,stdin); // Get data from stdin

        if (!strcmp(buffer,Q)) break; // If user types 'quit' exit the program

        strcat(buffer,EOL); // Concat data with iol newline escape sequence

        send(sock,buffer,strlen(buffer),0); // Send data to socket
        
    }

    close(sock);
    exit(0);
}
