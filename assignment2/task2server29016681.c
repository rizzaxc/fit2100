/* 
 * Student Name: Thach K. Tran
 * StudentID: 29016681
 * Start Date: 25/09/2018
 * End Date: 27/09/2018
 */


/* 
 * Server implimentation for 1-1 communication between a server and a client
 * The server creates a connection and responses to a client connected to it
 * It prints out data from the client to stdout along with the client name
 * This has a shutdown function
 */


#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SOCKETNAME "/tmp/a2-29016681.socket"
#define MAX_SIZE 1024
#define EOL "</n>"


int main(int argc, char const *argv[])
{
    char buffer[MAX_SIZE], clientName[MAX_SIZE];
    int n,sock,nsock,len,tmp;
    struct sockaddr_un name;
    fd_set set;

    /* Create a socket */
    if ((sock = socket(AF_UNIX,SOCK_STREAM,0)) < 0)
    {
        perror("server: socket");
        exit(1);
    }

    /* Create the address of the server */
    memset(&name,0,sizeof(struct sockaddr_un));
    name.sun_family = AF_UNIX;
    strcpy(name.sun_path,SOCKETNAME);
    len = sizeof(name.sun_family) + strlen(name.sun_path);

    /* Remove any previous socket */
    unlink(name.sun_path);

    /* Bind socket to the address */
    if (bind(sock,(struct sockaddr*) &name,SUN_LEN(&name)) < 0)
    {
        perror("server: bind");
        exit(1);
    }

    /* Listen for connections */
    if (listen(sock,5) < 0)
    {
        perror("server: listen");
        exit(1);
    }

    /* Accept a connection */
    if ((nsock = accept(sock,(struct sockaddr *) &name,&len)) < 0)
    {
        perror("server: accept");
        exit(1);
    }

    /* Exit button */
    printf("<button {Shutdown</n>} onclick=<putln {Q}>></n>\n");

    /* Setup a GUI window */
    printf("<box width=100,{%%} {");


    tmp = recv(nsock,buffer,MAX_SIZE,0); // Read from socket
    strncpy(clientName,buffer,tmp); // Store the name in the string clientName
    clientName[tmp] = '\0'; // Null terminate the name string
    printf("<titleBox:box color={yellow} background-color={blue} bold=true\
    width=100,{%%} {%s}>",clientName); // Create a box to hold the client name

    /* Write data from socket to stdout */
    while (1)
    {
        FD_ZERO(&set); // Initialize the fd set to be empty
        FD_SET(0,&set); // Add stdin to the set
        FD_SET(nsock,&set); // Add the socket to the set
        if (select(FD_SETSIZE,&set,NULL,NULL,NULL) < 0)
        {
            perror("server: select");
            close(nsock);
            close(sock);
            exit(1);
        }
        if (FD_ISSET(nsock,&set))
        {
            while ((tmp = recv(nsock,buffer,MAX_SIZE,0)) > 0)
                write(1,buffer,tmp);
        }
        if (FD_ISSET(0,&set)) break; // User has pressed quit
    }


    /* Close the GUI window */
    printf("}>");

    close(nsock);
    close(sock);
    exit(0);
}
