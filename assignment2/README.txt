*****Server and Client Communication, written in C*****
    ***By Thach K Tran, StudentID 29016681***
        ***Completed 07/10/2018***

This project involves client and server sending data to each other.



- Included:
    + Relevant codes for the client and 3 versions of the server
    + Bash script to run server v3 in TTY 8

- Compile & Launch:
    + Open your terminal and change dir to the folder
    + To compile the server version 1, type: 'gcc task1server29016681.c -o task1server29016681'
    + Do the same for version 2 and 3, adjusting the task number
    + To compile the client, type: 'gcc client29016681.c -o client29016681' (there's only 1 client version)
    + Run the server by typing 'iol -- ./task1server29016681' and so on for 3 versions
    + Run the client by typing './client29016681' 
    + Be sure to run the server first
    + You can also run server v3 in TTY 8 by typing 'sudo ./runstandalone.sh', entering your password
      and then switching your terminal to TTY 8 to interact with it (you need to switch back to run the client)
    + To quit the server, press the shutdown button in version 2 and 3. For version 1, just close the window
    + To quit the client, type 'quit' (this also means that you can't send the string 'quit' to the server)

- Client Information:
    + One program will try to establish a connection with a server, and sends over any data received from the 
    screen to the server.
    + You need to have a server running first

- Server Information:
    + Version 1: The server will listen to 1 client program and print any data received from it to the screen
                 If the client disconnects, the server will terminate.
    + Version 2: Same as version 1 with a shutdown button added to quit the server. This doesn't terminate when the client quits.
    + Version 3: Same as version 2, but now supports up to 5 clients at a time
                 If one client disconnects, another one could join

- Known issues:
    + In server v2 the shutdown won't appear at first (when there's no connection). Must have something to do with IOL
    + If the client sends too much data over when the server hasn't processed the client name, data won't appear until user sends
      the next data package. Also, there can be error regarding seperating client name and actual data (personally I haven't encountered this)