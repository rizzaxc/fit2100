#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h> 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
    int n, infile, outfile;
    char buffer[1024];

    if (argc != 3)
    {
        write(2,"Usage: appendfile file1 file2\n",30);
        exit(1);
    }

    // Open the first file for reading
    if ((infile = open(argv[1],O_RDONLY)) < 0)
    {
        perror(argv[1]);
        exit(1);
    }

    // Open the second file for writing
    if ((outfile = open(argv[2],O_WRONLY | O_APPEND)) < 0)
    {
        perror(argv[2]);
        exit(1);
    }

    // Append file1 to file2
    int byteRead;
    while ((byteRead = read(infile,buffer,1024)))
    {
        write(outfile,buffer,byteRead);

    }

    close(infile);
    close(outfile);
    exit(0);
}