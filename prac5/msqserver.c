#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MSQKEY 34858
#define MSQSIZE 32

struct msgbuf
{
    long mtype;
    char mtext[MSQSIZE];
};

int main(int argc, char const *argv[])
{
    key_t key;
    int n, msqid;
    struct msgbuf mbuf;

    /*
    * Create a new messgae  key. IPC_CREAT is used to create it,
    * and IPC_EXCL to make sure it does not exist already.
    * If you get an error on this, something on your system is
    * using the same key - change MSQKEY to something else.
    */
    
    key = MSQKEY;
    if ((msqid = msgget(key, IPC_CREAT | IPC_EXCL | 0666)) < 0)
    {
        perror("server: msgget");
        exit(1);
    }

    /*
    * Receive messages from the queue.
    * Messages of type 1 are to be printed on the stdout;
    * a message of type 2 indicates no more data.
    */

   while ((n = msgrcv(msqid, &mbuf, MSQSIZE, 0, 0)) > 0)
   {
       if (mbuf.mtype == 1)
       {
           //TODO
           write(1,&mbuf.mtext,sizeof(mbuf.mtext));
       }
       else if (mbuf.mtype == 2)
       {
           // Remove the message queue from the system
           if (msgctl(msqid, IPC_RMID, (struct msqid_ds *) 0) < 0)
           {
               perror("server: msgctl");
               exit(1);
           }
       }
   }
   exit(0);
}
