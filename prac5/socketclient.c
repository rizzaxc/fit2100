#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SOCKETNAME "/tmp/a2-29016681.socket"

int main(int argc, char const *argv[])
{
    char buffer[1024];
    int n,sock,len;
    struct sockaddr_un name;

    // Create a socket
    if ((sock = socket(AF_UNIX,SOCK_STREAM,0)) < 0)
    {
        perror("client: socket");
        exit(1);
    }

    // Create the address of the server
    memset(&name,0,sizeof(struct sockaddr_un));
    name.sun_family = AF_UNIX;
    strcpy(name.sun_path,SOCKETNAME);
    len = sizeof(name.sun_family) + strlen(name.sun_path);

    // Connect to the server
    if (connect(sock,(struct sockaddr *) &name,SUN_LEN(&name)) < 0)
    {
        perror("client: connect");
        exit(1);
    }

    // TODO: Send name. Read from stdin and send the data to the socket
    // null termination
    write(sock,argv[0],sizeof(argv[0]));
    int tmp;
    tmp = read(0,buffer,1024);
    write(sock,buffer,tmp);

    close(sock);
    exit(0);
}
