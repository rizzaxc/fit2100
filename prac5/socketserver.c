#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SOCKETNAME "/tmp/a2-29016681.socket"

int main(int argc, char const *argv[])
{
    char buffer[1024];
    int n,sock,nsock,len;
    struct sockaddr_un name;

    // Create a socket
    if ((sock = socket(AF_UNIX,SOCK_STREAM,0)) < 0)
    {
        perror("server: socket");
        exit(1);
    }

    // Create the address of the server
    memset(&name,0,sizeof(struct sockaddr_un));
    name.sun_family = AF_UNIX;
    strcpy(name.sun_path,SOCKETNAME);
    len = sizeof(name.sun_family) + strlen(name.sun_path);

    // Remove any previous socket
    unlink(name.sun_path);

    // Bind socket to the address
    if (bind(sock,(struct sockaddr*) &name,SUN_LEN(&name)) < 0)
    {
        perror("server: bind");
        exit(1);
    }

    // Listen for connections
    if (listen(sock,5) < 0)
    {
        perror("server: listen");
        exit(1);
    }

    // Accept a connection
    if ((nsock = accept(sock,(struct sockaddr *) &name,&len)) < 0)
    {
        perror("server: accept");
        exit(1);
    }

    // TODO: Read from new socket until EOF and print all data to stdout
    // null termination
    int tmp;
    tmp = read(nsock,buffer,512);
    write(1,buffer,tmp);
    while ((tmp = read(nsock,buffer,512))>0)
    {
        write(1,buffer,tmp);
    }

    close(nsock);
    close(sock);
    exit(0);
}
